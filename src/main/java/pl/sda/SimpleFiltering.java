package pl.sda;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class SimpleFiltering {
    private static List<Integer> myInts = Arrays.asList(0, 5, 10, 15, 20, 25, 30);

    public static void main(String[] args) {
        final int sum = myInts.stream().mapToInt(e -> e).sum();
        final OptionalDouble avg = myInts.stream().mapToInt(e -> e).average();
        System.out.println("Suma: " + sum);
        System.out.println("Srednia: " + avg);
    }
}
