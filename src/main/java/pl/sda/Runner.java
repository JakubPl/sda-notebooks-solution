package pl.sda;

import pl.sda.enums.Brand;
import pl.sda.model.Notebook;

import java.time.Year;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Runner {
    private final static Set<Notebook> notebooks = new HashSet<>();
    private static final Predicate<Notebook> DELL_BRAND_PREDICATE = notebook -> notebook.getBrand().equals(Brand.DELL);
    private static final Year YEAR_2000 = Year.of(2000);

    static {
        //to jest statyczny blok inicjalizacyjny, wykona sie tylko raz w calej aplikacji
        notebooks.add(new Notebook("Very old name", Brand.ACER, Year.of(1990), 5.0, false));
        notebooks.add(new Notebook("L702", Brand.DELL, Year.of(2007), 100.0, false));
        notebooks.add(new Notebook("XPS 15", Brand.DELL, Year.of(2018), 10000.0, true));
        notebooks.add(new Notebook("Galaxy Book 12", Brand.SAMSUNG, Year.of(2016), 3199.0, true));
        notebooks.add(new Notebook("MacBook Pro", Brand.APPLE, Year.of(2018), 12099.0, true));
        notebooks.add(new Notebook("MacBook", Brand.DELL, Year.of(2018), 5000, true));
        notebooks.add(new Notebook("MacBook Air", Brand.DELL, Year.of(2018), 6000, true));
    }

    public void run() {
        System.out.println("Wyświetl wszystkie laptopy, które nie są marki Dell");
        notebooks.stream()
                .filter(DELL_BRAND_PREDICATE.negate())
                .forEach(System.out::println);

        System.out.println("Wyświetl laptopy wyprodukowane po 2000");
        notebooks.stream()
                .filter(notebook -> notebook.getYearOfProduction().isAfter(YEAR_2000))
                .forEach(System.out::println);

        System.out.println("Wyświetl laptopy, których nazwa ma mniej niż 5 znaków");
        notebooks.stream()
                .filter(notebook -> notebook.getName().length() < 5)
                .forEach(System.out::println);

        System.out.println("Odfiltruj niedostępne laptopy i wyświetl pozostałe");
        notebooks.stream()
                .filter(Notebook::isAvailable)
                .forEach(System.out::println);

        System.out.println("Zsumuj ceny wszystkich laptopów firmy Dell");
        final double dellSum = notebooks.stream()
                .filter(DELL_BRAND_PREDICATE)
                .mapToDouble(Notebook::getPrice)
                .sum();
        System.out.println(dellSum);

        System.out.println("Sprawdź jaka jest średnia cena laptopów Apple");
        notebooks.stream()
                .filter(e -> Brand.APPLE.equals(e.getBrand()))
                .mapToDouble(Notebook::getPrice)
                .average()
                .ifPresent(System.out::println);

        System.out.println("Wyświetl tylko laptopy, których nazwa firmy ma więcej niż 4 znaki");
        notebooks.stream()
                .filter(e -> Brand.APPLE.name().length() > 4)
                .forEach(System.out::println);

    }
}
